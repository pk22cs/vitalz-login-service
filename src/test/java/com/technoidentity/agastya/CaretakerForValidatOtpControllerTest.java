package com.technoidentity.agastya;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.technoidentity.agastya.service.CustomStafftUserDetailService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@TestMethodOrder(OrderAnnotation.class)
public class CaretakerForValidatOtpControllerTest {
/*
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	CustomStafftUserDetailService customUserDetailService1;
	/*@Test
	@Order(3)
	public void generateTokenTest() throws Exception {

		PatientDetails user = customUserDetailService1.getUserDetails("7415520049");
		int otp = user.getOtp();

		MvcResult result = mvc.perform(post("/agastya/caretaker/login").header("Content-Type", "application/json")
				 .contentType(MediaType.APPLICATION_JSON)
				.content("{\"otp\":"+otp+"}")).andReturn();
		MockHttpServletResponse resp = result.getResponse();
		assertNotNull(resp.getContentAsString());
		assertEquals(HttpStatus.OK.value(), resp.getStatus());

	}*/
/*
	@Test
	@Order(4)
	public void generateTokenTest2() throws Exception {

		MvcResult result = mvc.perform(post("/agastya/caretaker/login").header("Content-Type", "application/json")
				// .contentType(MediaType.APPLICATION_JSON)
				.content("{\"otp\":753508}")).andReturn();
		MockHttpServletResponse resp = result.getResponse();
		assertNotNull(resp.getContentAsString());
		assertEquals(HttpStatus.BAD_REQUEST.value(), resp.getStatus());

	}
*/
	
}
