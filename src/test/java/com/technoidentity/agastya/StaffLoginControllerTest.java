package com.technoidentity.agastya;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@TestMethodOrder(OrderAnnotation.class)
public class StaffLoginControllerTest {
	/*
	@Autowired
	private MockMvc mvc;
	
	
/*	@Test
	@Order(1)
	public void generateToken() throws Exception {
 
	
		MvcResult result = mvc
				.perform(post("/agastya/staff/login")
						.header("Content-Type", "application/json")
						//.contentType(MediaType.APPLICATION_JSON)
						.content("{\"userId\":\"VITALZ-1\",\"password\":\"12345\"}"))
				.andReturn();
		MockHttpServletResponse resp = result.getResponse();
		assertNotNull(resp.getContentAsString());
		assertEquals(HttpStatus.OK.value(), resp.getStatus());
      
	}
	
	
	@Test
	@Order(2)
	public void generateToken2() throws Exception {
 
	
		MvcResult result = mvc
				.perform(post("/agastya/staff/login")
						.header("Content-Type", "application/json")
						//.contentType(MediaType.APPLICATION_JSON)
						.content("{\"userId\":\"prince\",\"password\":\"1234\"}"))
				.andReturn();
		MockHttpServletResponse resp = result.getResponse();
		assertNotNull(resp.getContentAsString());
		assertEquals(401, resp.getStatus());
      
	}*/
	
}
