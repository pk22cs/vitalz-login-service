package com.technoidentity.agastya.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PatientDataList {
	
	
	private String name;
	private String patientId;
	private String emergencyContactNumber;
	private String emergencyContactName;
	private Gender gender;
	private int age;


}
