package com.technoidentity.agastya.dto;

public enum Role {

	doctor,nurse,admin,caretaker,patient;
}
