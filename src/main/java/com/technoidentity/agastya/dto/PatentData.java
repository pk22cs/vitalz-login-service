package com.technoidentity.agastya.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PatentData {

  private Boolean status;
  private String contactExtention;

}
