package com.technoidentity.agastya.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaretakerData {
	
	private String emergencyContactName;
	private String emergencyContactNumber;

}
