package com.technoidentity.agastya.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffDataRequest {

	private String userId;
	private String name;
	private int age;
	private Gender gender;
	private String phoneNo;
	private String phoneExt;
	private Role role;
	private String email;

}
