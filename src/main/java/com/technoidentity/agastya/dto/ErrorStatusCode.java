package com.technoidentity.agastya.dto;

public enum ErrorStatusCode {

   wrong_password,
   invalid_input, internal_server_error, invalid_email;
}
