package com.technoidentity.agastya.dto;

import com.technoidentity.agastya.model.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HospitalListResponse {

	private String id;
	private String hospitalName;
	private Address address;
	private Boolean status;
}
