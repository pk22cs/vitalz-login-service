package com.technoidentity.agastya.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ForgetPassword {
	
	private String userId;
	private String oldPassword;
	private String newPassword;
	
	

}
