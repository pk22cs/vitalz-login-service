package com.technoidentity.agastya.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.dto.StaffDataRequest;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.StaffDetails;
import com.technoidentity.agastya.model.SuccessResponse;
import com.technoidentity.agastya.repositery.StaffRepositery;
import com.technoidentity.agastya.service.EmailService;
import com.technoidentity.agastya.service.GeneratePasswordService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins="http://localhost:3000")
@RestController


public class AddStaffController {
	
	 @org.springframework.beans.factory.annotation.Autowired(required=true)
	  private BCryptPasswordEncoder bCryptPasswordEncoder;
	 
	@Autowired
	private StaffRepositery repository;
	
	@Autowired
	private GeneratePasswordService generatePasswordService;
	
	@Autowired
	private EmailService service;
	
	
	@Operation(summary = "This method is use to Add Staff like DOCTOR and NURSE ")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully Staff Details is Added in Database ", content = { 
					 @Content(mediaType = "application/json" ,schema =@Schema(implementation =SuccessResponse.class))  }),
     
			
			
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))}),
			
			@ApiResponse(responseCode = "400", description = "Staff is already registered in DB ", content = {
					@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))})
			 })
	
	@PostMapping("/register-staff")
	public ResponseEntity<?> registerStaff(@RequestBody StaffDataRequest staffDataRequest){
		ErrorResponse response = new ErrorResponse(); 
		SuccessResponse status=new SuccessResponse();
		
		 System.out.println("user Registation");
	 try {
		 
		  StaffDetails staffData=new StaffDetails();
		
		   BeanUtils.copyProperties(staffDataRequest,staffData);
		 
		  String userId=staffDataRequest.getUserId();	
		  System.out.println(staffData.toString());
		
		StaffDetails temp=repository.findByUserId(userId);
		   
		  if(!(temp==null)) {
			    response.setStatusCode(ErrorStatusCode.invalid_input);
				response.setMessage("Staff is already registered in DB");
			   return new ResponseEntity<ErrorResponse>(response,HttpStatus.BAD_REQUEST);
		  }
		
		  String emailId=staffData.getEmail();
		  String password=generatePasswordService.generatePassword(8);
		 
	      String message="\n<h3>Your UserId : " + userId +"<br/> Password : "+ password +"</h3>\n";   
	       
	      String subject="Vitalz Registration Successful ";
	       //save password in DB
	      String msg="<h2>Your Registration is Successful please login with below userId & Password </h2> "+message+"\n\n\n thanks  ";	
	      
	       staffData.setPassword(bCryptPasswordEncoder.encode(password));
	       repository.insert(staffData);
	    	
	    	//sendEmail();
	    	service.sendEmail(subject,emailId,msg);
	    	
	        System.out.println("Done");
		  
	 }
	 catch(Exception e) {
		 e.printStackTrace();
		    response.setStatusCode(ErrorStatusCode.internal_server_error);
			response.setMessage("Something Wrong in server side");
		 return new ResponseEntity<ErrorResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
	 }
		
	    status.setSuccess(true);
	    status.setReason("Registration is Successfully..? ");
		return new ResponseEntity<SuccessResponse>(status,HttpStatus.OK);
	}

}
