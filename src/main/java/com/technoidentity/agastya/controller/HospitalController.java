package com.technoidentity.agastya.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.dto.HospitalListResponse;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.Hospital;
import com.technoidentity.agastya.model.SmsPojo;
import com.technoidentity.agastya.repositery.HospitalRepositery;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins = "http://localhost:3000")
@RestController

public class HospitalController {

	@Autowired
	private WebClient.Builder webClintBuilder;

	@Autowired
	private HospitalRepositery hospitalRepositery;

	@Operation(summary = "This will fetch List of  Hospital name")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully fetch All Hospital Name from Database ", content = { 
					@Content(mediaType = "application/json" ,array = @ArraySchema(schema =@Schema(implementation =HospitalListResponse.class)) ) }),

			@ApiResponse(responseCode = "204", description = "No one Hospital is avilable in Database", content = {
					@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))})
	})

	@PostMapping("/hospital-name")
	public ResponseEntity<?> getHospitalName(@RequestBody SmsPojo sms) {
		ErrorResponse response=new ErrorResponse();
		List<Hospital> hospitalList=null;
		List<HospitalListResponse> hospitalListResponses=new ArrayList<>();
		try {

			hospitalList=hospitalRepositery.findAll();

			// verify the Number is Available in DB or NOT
            String emergencyContactNumber=sms.getPhoneNo();
			Set<String> listOfHospitalId=webClintBuilder.build()
					.get()   
					.uri("http://20.204.14.2/graph/hospital-list/"+emergencyContactNumber)
					.retrieve()
					.bodyToMono(Set.class)
					.block();
			
			Iterator<Hospital> itr=hospitalList.iterator();
			while(itr.hasNext()) {

				Hospital hospital=itr.next();

				String hospitalId=hospital.getId();

			    Boolean status=listOfHospitalId.contains(hospitalId);
			    
			    HospitalListResponse data=new HospitalListResponse();
				BeanUtils.copyProperties(hospital, data);
				data.setStatus(status);

				hospitalListResponses.add(data);

			}


			





		}catch(Exception e) {
			response.setStatusCode(ErrorStatusCode.invalid_input);
			response.setMessage("No Record Found");
			e.printStackTrace();
			return new ResponseEntity<ErrorResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<HospitalListResponse>>(hospitalListResponses,HttpStatus.OK);
	}
}
