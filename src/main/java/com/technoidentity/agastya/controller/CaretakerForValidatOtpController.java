package com.technoidentity.agastya.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.technoidentity.agastya.dto.CaretakerData;
import com.technoidentity.agastya.dto.CaretakerResponceData;
import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.helper.JwtUtil1;
import com.technoidentity.agastya.helper.OtpService;
import com.technoidentity.agastya.model.CaretakerResponse;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.TempOTP;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;



@CrossOrigin(origins="http://localhost:3000")
@RestController


public class CaretakerForValidatOtpController {

	@Autowired
	private WebClient.Builder webClintBuilder;
	
	@Autowired
	private OtpService otpService;
	
	//It is Required Don't Remove this 
	@Autowired
	private AuthenticationManager authenticationManager;

	

	@Autowired
	private JwtUtil1 jwtutil;

	 @Operation(summary = "This Method is Use to validate the OTP If OTP is valid then send Response Token with USER Details")
		@ApiResponses(value = {
				@ApiResponse(responseCode = "200", description = "Successfully OTP is validated ", content = { 
						@Content(mediaType = "application/json",schema =@Schema(implementation =CaretakerResponse.class)) }),

				@ApiResponse(responseCode = "400", description = "Invalid OTP", content = {
						@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))}) ,
				
				
		       @ApiResponse(responseCode = "500", description = "Invalid OTP", content = {
				 @Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))}) })

	
	@RequestMapping(value="/caretaker/login",method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@RequestBody TempOTP otp ) throws Exception{
		ErrorResponse responseData=new ErrorResponse(); 
        ResponseEntity<?> responce = null;
        System.out.println("OTP verification");
       //validate the OTP given by endUser
		 String regex = "^[0-9]{6}$";
		 String tempOTP=Integer.toString( otp.getOtp());
		 
	 if(tempOTP.matches(regex)) {
		try {
			//Fetch User From DB with OTP
			int otpnumber=otp.getOtp();
			String phoneNumber=otp.getPhoneNo();
			
			
			
			 CaretakerData data=webClintBuilder.build()
						.get()
						.uri("http://20.204.14.2/graph/caretaker/"+phoneNumber)
						.retrieve()
						.bodyToMono(CaretakerData.class)
						.block();
			 CaretakerResponceData user=new CaretakerResponceData();
			 user.setName(data.getEmergencyContactName());
			 user.setPhoneNo(data.getEmergencyContactNumber());
			 
			 System.out.println(user.toString());
			
			//Validate the OTP
			int  serverOTP=otpService.getOtp(phoneNumber);
			if(serverOTP==otpnumber) {
			
				otpService.clearOTP(phoneNumber);
				
			//Generate Token based on Mobile No
				 System.out.println("Hello token generater-1");
		         String token=this.jwtutil.generateToken(user);
		         System.out.println("Hello token generater-2");
		         System.out.println(token);
		         
		         
		        //Make the Response Object
		        /* UserDetailsResponse user=new UserDetailsResponse();
		         user.setId(userDetail.getUserId());
		         user.setName(userDetail.getName());
		         user.setEmail(userDetail.getEmail());
		         user.setRole(userDetail.getRole());
		         user.setPhoneNo(userDetail.getPhoneNo());
		         user.setPhoneExt(userDetail.getPhoneExt());*/
		          
		          
				responce=ResponseEntity.ok(new CaretakerResponse(token,user));
				
			}
			else {
				responseData.setStatusCode(ErrorStatusCode.invalid_input);
		        responseData.setMessage("Please enter correct OTP");
				System.out.println("OTP is Not Valid");
				return new  ResponseEntity<ErrorResponse>(responseData,HttpStatus.BAD_REQUEST);
			}
			
		}
		catch(Exception e) {
			responseData.setStatusCode(ErrorStatusCode.invalid_input);
	        responseData.setMessage("Please enter correct OTP");
			 e.printStackTrace();
			return new  ResponseEntity<ErrorResponse>(responseData,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	else {
		responseData.setStatusCode(ErrorStatusCode.invalid_input);
        responseData.setMessage("Invalid OTP");
		return new  ResponseEntity<ErrorResponse>(responseData,HttpStatus.BAD_REQUEST);
	  }
		return responce;
	}
}
