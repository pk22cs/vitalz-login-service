package com.technoidentity.agastya.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.dto.PatientDataList;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.RequestData;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class PatientCaretakerController {

	@Autowired
	private WebClient.Builder webClintBuilder;
	
	
	@Operation(summary = "This will fetch List of Patient Detailas base on Hospital Name")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully fetch All Patient Details from Database ", content = { 
					 @Content(mediaType = "application/json" ,array = @ArraySchema(schema =@Schema(implementation =PatientDataList.class)) ) }),
     
			
			
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))}),
			
			@ApiResponse(responseCode = "400", description = "Invalid Mobile Number", content = {
					@Content(mediaType = "application/json",schema =@Schema(implementation =ErrorResponse.class))})
			 })
	
	@RequestMapping(value="/patient-caretakers",method = RequestMethod.POST)
	public ResponseEntity<?> patitentList(@RequestBody RequestData data){
		System.out.println("List of patients");
		ErrorResponse response=new ErrorResponse();
		
		System.out.println(data.toString());
		
		// List<PatientListResponse> hospitalPatient=new ArrayList<PatientListResponse>();
		 List<PatientDataList> patientList=null;
		
		 String phoneNumber=data.getPhoneNo();
		 String hospitalId=data.getHospitalId();
		 
		 String regex = "(0/91)?[7-9][0-9]{9}";
	 if(phoneNumber.matches(regex)) {
		 try {
			System.out.println("phone");
			//List<PatientDetails> patientList=patientRepositery.findAll();
			//find list of patient in particular hospital
			
			patientList=webClintBuilder.build()
						.get()
						.uri("http://20.204.14.2/graph/patient-list/"+hospitalId+"/"+phoneNumber)
						.retrieve()
						.bodyToMono(List.class)
						.block();
			
			
			
			System.out.println(patientList.toString());
			
			System.out.println("phone");
			
			
		}catch(Exception e) {
			 response.setStatusCode(ErrorStatusCode.invalid_input);
			 response.setMessage("Something wrong in server side error");
			e.printStackTrace();
			return new ResponseEntity<ErrorResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	 }
	 else {
			 response.setStatusCode(ErrorStatusCode.invalid_input);
			 response.setMessage("Invalid Mobile Number");
			 return new ResponseEntity<ErrorResponse>(response,HttpStatus.BAD_REQUEST); 
		 }
       
		 return new ResponseEntity<List<PatientDataList>>(patientList,HttpStatus.OK);
	}

}
