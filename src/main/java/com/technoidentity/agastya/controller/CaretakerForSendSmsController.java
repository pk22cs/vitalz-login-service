package com.technoidentity.agastya.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.technoidentity.agastya.dto.PatentData;
import com.technoidentity.agastya.helper.OtpService;
import com.technoidentity.agastya.model.SmsPojo;
import com.technoidentity.agastya.model.SuccessResponse;
import com.technoidentity.agastya.service.SmsService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class CaretakerForSendSmsController {

	@Autowired
	private WebClient.Builder webClintBuilder;
	
	@Autowired
	private OtpService otpService;
	
	@Autowired
	SmsService service;

	@Autowired
	private SimpMessagingTemplate webSocket;

	private final String TOPIC_DESTINATION = "/lesson/sms";

	
	
	 @Operation(summary = "This Method is Use to validate the Mobile No. If Mobile No is available In DB then Send OTP 6-digit ")
		@ApiResponses(value = {
				@ApiResponse(responseCode = "200", description = "Successfully OTP Send Given Mobile No ", content = { 
						@Content(mediaType = "application/json",schema =@Schema(implementation =SuccessResponse.class)) }),
				@ApiResponse(responseCode = "400", description = "if your mobile number is not valid format : Invalid Mobile No  ", content = { 
						@Content(mediaType = "application/json",schema =@Schema(implementation =SuccessResponse.class)) }),

				@ApiResponse(responseCode = "500", description = "Invalid Mobile No. It is not available in DB", content = {
						@Content(mediaType = "application/json",schema =@Schema(implementation =SuccessResponse.class))}),
				
				@ApiResponse(responseCode = "400", description = "\"This Number is not avilable in DB\"", content = {
						@Content(mediaType = "application/json",schema =@Schema(implementation =SuccessResponse.class))})
				})
	
	
	
	// You can send SMS in verified Number
	@RequestMapping(value = "/caretaker/send-otp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> smsSubmit(@RequestBody SmsPojo sms) {
		 SuccessResponse responseData=new SuccessResponse();
		System.out.println("this is mobile number verification");
		 String regex = "(0/91)?[7-9][0-9]{9}";
		 String phoneNumber=sms.getPhoneNo();
		 if(phoneNumber.matches(regex)) {
		 try {
			System.out.println("good");
			// verify the Number is Available in DB or NOT
			 PatentData data=webClintBuilder.build()
						.get()
						.uri("http://20.204.14.2/graph/verify/"+phoneNumber)
						.retrieve()
						.bodyToMono(PatentData.class)
						.block();	
			 System.out.println("good");
			 System.out.println(data);
	       //PatientDetails user = customUserDetailService1.getUserDetails(phoneNo);
			if ( data== null) {
				
				responseData.setSuccess(false);
		        responseData.setReason("This Number is not avilable in DB");
			    return new ResponseEntity<SuccessResponse>(responseData, HttpStatus.BAD_REQUEST);
			} 
			
			// If Number is available then send OTP in Mobile Number
			
			int otp =otpService.generateOTP(phoneNumber);

		   String msg = "Your OTP - " + otp
					+ " please verify this OTP in your Application by Er Prince kumar Technoidentity.com";
		   
            String phonenumber=data.getContactExtention()+phoneNumber;
        	
			service.send(phonenumber, msg);

		} catch (Exception e) {
			responseData.setSuccess(false);
	        responseData.setReason("please enter correct Mobile Number");
			return new ResponseEntity<SuccessResponse>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
		 else {
			    responseData.setSuccess(false);
		        responseData.setReason("Invalid Mobile Number");
			 return new ResponseEntity<SuccessResponse>(responseData, HttpStatus.BAD_REQUEST);
		 }

		webSocket.convertAndSend(TOPIC_DESTINATION, getTimeStamp() + ": SMS has been sent!: " + sms.getPhoneNo());
        responseData.setSuccess(true);
        responseData.setReason("OTP sent your  Mobile number Successfully");
		return new ResponseEntity<SuccessResponse>( responseData, HttpStatus.OK);
	}
	
	
/*
	@RequestMapping(value = "/smscallback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void smsCallback(@RequestBody MultiValueMap<String, String> map) {
		service.receive(map);
		webSocket.convertAndSend(TOPIC_DESTINATION,
				getTimeStamp() + ": Twilio has made a callback request! Here are the contents: " + map.toString());
	}
*/
	private String getTimeStamp() {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
	}

}
