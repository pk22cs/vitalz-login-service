package com.technoidentity.agastya.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoidentity.agastya.dto.Email;
import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.StaffDetails;
import com.technoidentity.agastya.model.SuccessResponse;
import com.technoidentity.agastya.repositery.StaffRepositery;
import com.technoidentity.agastya.service.EmailService;
import com.technoidentity.agastya.service.GeneratePasswordService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins = "http://localhost:3000")
@RestController


public class ForgetPasswordController {

	@org.springframework.beans.factory.annotation.Autowired(required = true)
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private StaffRepositery repository;

	@Autowired
	private GeneratePasswordService generatePasswordService;

	@Autowired
	private EmailService service;

	@Operation(summary = "This method is use to reset the password ")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully password is reset and send in emailID ", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = SuccessResponse.class)) }),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid email Id please enter correct email ID ", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }),

			@ApiResponse(responseCode = "404", description = "this email ID is not avilable in DB ", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }) })
	
	@PostMapping(value = "/forget-password")
	public ResponseEntity<?> sendEmail123(@RequestBody Email emailData) {

		ErrorResponse response = new ErrorResponse();
		SuccessResponse status=new SuccessResponse();
		StaffDetails data = null;
		System.out.println("Sending Email...");
		String email = emailData.getEmailId();

		System.out.println(email);
		String pattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
		if (email.matches(pattern)) {
			try {
				System.out.println("Hello");
				data = repository.findByEmail(email);

				System.out.println(data);
				if (data == null) {
					response.setStatusCode(ErrorStatusCode.invalid_input);
					response.setMessage("This email is not avilable in Database");
					return new ResponseEntity<ErrorResponse>(response, HttpStatus.NOT_FOUND);
				}
				// Email is Avilable in DB
				String password = generatePasswordService.generatePassword(8);
				String subject = "Forget your password";
				String message = "<h3>\nYour UserId : " + data.getUserId() + "<br/> Password : " + password
						+ "\n</h3>\n";

				String msg = "<h1>Hi User \n Your password is Successfully reset please login with below userId & Password\n </h1>"
						+ message + "\n<button><a href=\"https://javaforjob1.blogspot.com\">click hear</a></button>"
						+ "\n Thanks for using our service.\n by Prince Kumar";

				// sendEmail();
				service.sendEmail(subject, email, msg);

				// save password in DB
				data.setPassword(bCryptPasswordEncoder.encode(password));
				repository.save(data);

				System.out.println("Done");
			} catch (Exception e) {
				e.printStackTrace();
				response.setStatusCode(ErrorStatusCode.internal_server_error);
				response.setMessage("Something error is occured in server side");
				return new ResponseEntity<ErrorResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} 
		else {
			response.setStatusCode(ErrorStatusCode.invalid_email);
			response.setMessage("Invalid Email please enter correct email");
			return new ResponseEntity<ErrorResponse>(response, HttpStatus.BAD_REQUEST);
		}

		status.setSuccess(true);
		status.setReason("Successful your password is Reset");
		return new ResponseEntity<SuccessResponse>(status, HttpStatus.OK);
	}

}
