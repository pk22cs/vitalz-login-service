package com.technoidentity.agastya.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoidentity.agastya.dto.ErrorStatusCode;
import com.technoidentity.agastya.dto.ForgetPassword;
import com.technoidentity.agastya.model.ErrorResponse;
import com.technoidentity.agastya.model.StaffDetails;
import com.technoidentity.agastya.model.SuccessResponse;
import com.technoidentity.agastya.repositery.StaffRepositery;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins="http://localhost:3000")
@RestController


public class ResetPasswordController {
	
	
	@org.springframework.beans.factory.annotation.Autowired(required = true)
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private StaffRepositery staffRepository;
	
	
	@Operation(summary = "This method is use to change the the password ")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully password is change please login with your password", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = SuccessResponse.class)) }),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid Password", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }),

			@ApiResponse(responseCode = "404", description = "Invalid UserId  ", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)) }) })
	
	@PutMapping("/resetPassword")
	public ResponseEntity<?> forgetPassword(@RequestBody ForgetPassword passwordData ){
		
		ErrorResponse response = new ErrorResponse();
		SuccessResponse status=new SuccessResponse();
		try {
			
			String userId=passwordData.getUserId();
			StaffDetails data= staffRepository.findByUserId(userId);
			if(data==null) {
				response.setStatusCode(ErrorStatusCode.invalid_input);
				response.setMessage("Invalid UserId please enter correct");
				return new ResponseEntity<ErrorResponse>(response, HttpStatus.NOT_FOUND);
					
				
			}
			
			//it's means that userId is avilable in DB
			//now we have chech DB password and Given Password Both are equal or not
			String dbPassword=data.getPassword();
			String oldPassword=passwordData.getOldPassword();
			if(bCryptPasswordEncoder.matches(oldPassword, dbPassword)) {
				
				//Encript the new password and insert in DB
				String newPassword=bCryptPasswordEncoder.encode(passwordData.getNewPassword());
				
				data.setPassword(newPassword);
				
				staffRepository.save(data);
				
			}
			else{
				
				response.setStatusCode(ErrorStatusCode.invalid_input);
				response.setMessage("Invalid Password please enter correct");
				return new ResponseEntity<ErrorResponse>(response, HttpStatus.BAD_REQUEST);
			}
			
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
			response.setStatusCode(ErrorStatusCode.internal_server_error);
			response.setMessage("Someting Wrong in server side");
			return new ResponseEntity<ErrorResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
		}
		status.setSuccess(true);
		status.setReason("Successfully Password is change go to login page");
		return  new ResponseEntity<SuccessResponse>(status,HttpStatus.OK);
	}

}
