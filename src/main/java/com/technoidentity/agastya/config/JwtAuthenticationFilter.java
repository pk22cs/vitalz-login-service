package com.technoidentity.agastya.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.technoidentity.agastya.helper.JwtUtil;
import com.technoidentity.agastya.helper.JwtUtil1;
import com.technoidentity.agastya.model.PatientDetails;
import com.technoidentity.agastya.service.CustomStafftUserDetailService;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	

	@Autowired
	private CustomStafftUserDetailService CustomUserDetailService;

	@Autowired
	private JwtUtil1 jwtUtil1;
	
	@Autowired
	private JwtUtil jwtUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		System.out.println("first filter");
		String requestTokenHeader=request.getHeader("Authorization");
		
        if(requestTokenHeader!=null && requestTokenHeader.startsWith("Bearer ")) 
        {
			String userId=null;
			String jwtToken=null;
			String phoneNo = null;
			
			jwtToken=requestTokenHeader.substring(7);
  
			 System.out.println("token validate-1");
			 
			 String role=this.jwtUtil.extractRole(jwtToken);
			 System.out.println(role);

			if("doctor".equalsIgnoreCase(role)||"nurse".equalsIgnoreCase(role)||"admin".equalsIgnoreCase(role)) 
			{

				 System.out.println("Username password-1");
				userId=this.jwtUtil.extractUsername(jwtToken);	

				UserDetails userDetails=this.CustomUserDetailService.loadUserByUsername(userId);
             
				//validate the token
				
				if(userId!=null && SecurityContextHolder.getContext().getAuthentication()==null) {

					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities());	
					usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

				}
			}	
			else {
					
					 System.out.println("Phone Number OTP");
					phoneNo = this.jwtUtil1.extractPhoneNo(jwtToken);
					
					//PatientDetails userDetails1=this.customUserDetailService1.getUserDetails(phoneNo); //problem


					System.out.println("7---------------------"); 
					
					if(phoneNo!=null && SecurityContextHolder.getContext().getAuthentication()==null) {
/*
						UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=new
								
								UsernamePasswordAuthenticationToken(userDetails1,
										userDetails1.getPhoneNumber(),userDetails1.getAuthorities());
						
						
						usernamePasswordAuthenticationToken.setDetails(new
								WebAuthenticationDetailsSource().buildDetails(request));
						SecurityContextHolder.getContext().setAuthentication(
								usernamePasswordAuthenticationToken);
                           System.out.println("good filter");
                           
                           
                           
                           HttpSession session=request.getSession();  
                           session.setAttribute("userId","prince kumar ojha jee");
                   */

				 }  
				}

			



		}
		
      System.out.println("Filter complete");
     
		filterChain.doFilter(request, response);
		}
}
