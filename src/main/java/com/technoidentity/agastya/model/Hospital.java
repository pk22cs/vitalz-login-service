package com.technoidentity.agastya.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "Hospital_details")
@Component
public class Hospital {

	@Id
	private String id;
	private String hospitalName;
	private Address address;
}
