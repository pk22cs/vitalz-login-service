package com.technoidentity.agastya.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestData {
	
	private String hospitalId;
	private String phoneNo;
	
	
}
