package com.technoidentity.agastya.model;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import com.technoidentity.agastya.dto.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value="Staff_details")
@Component
public class StaffDetails {
	
	@Id
	//@Field(value="userId")
	private String userId;
    private String name;
	private String password;
	private String email;
	private Role role;
	private String phoneNo;
	private String phoneExt;
	private int age;
	private String gender;

}
