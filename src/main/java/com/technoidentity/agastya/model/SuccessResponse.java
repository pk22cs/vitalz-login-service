package com.technoidentity.agastya.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuccessResponse {

	 private boolean isSuccess;
	 private String reason;
		
}
