package com.technoidentity.agastya.model;

import com.technoidentity.agastya.dto.ErrorStatusCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
	
	private ErrorStatusCode statusCode;
	private String message;
	
}
