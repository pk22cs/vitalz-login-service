package com.technoidentity.agastya.model;

import org.springframework.stereotype.Component;

import com.technoidentity.agastya.dto.CaretakerResponceData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class CaretakerResponse {
	
	
	private String token;
    private CaretakerResponceData  user;
	
	
	

}
