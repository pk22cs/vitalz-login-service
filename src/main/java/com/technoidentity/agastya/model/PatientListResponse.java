package com.technoidentity.agastya.model;



import com.technoidentity.agastya.dto.Gender;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientListResponse {
	
	
	private String id;
	private String name;
	private String address;
	private int age;
	private Gender gender;

}
