package com.technoidentity.agastya.model;

import com.technoidentity.agastya.dto.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailsResponse {
	
	private String id;
    private String name;
    private String email;
    private Role role;
    private String phoneNo;
    private String phoneExt;

}
