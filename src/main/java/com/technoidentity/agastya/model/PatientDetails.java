package com.technoidentity.agastya.model;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.technoidentity.agastya.dto.Gender;
import com.technoidentity.agastya.dto.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class PatientDetails {
	
	
	private String patientId;
    private String name;
    private String email;
    private Role role;
	private String phoneNumber;
	private String phoneExt;
	
	
	private String hospitalName;
	private String hospitalId;
	private String emergencyContractNo;
	
	private String address;
	private int    age;
	private Gender gender;
	
	
	
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}
	
	

}
