package com.technoidentity.agastya.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.technoidentity.agastya.model.CustomUserDetails;
import com.technoidentity.agastya.model.StaffDetails;
import com.technoidentity.agastya.repositery.StaffRepositery;

@Service
public class CustomStafftUserDetailService  implements UserDetailsService{

	
	@Autowired(required = true)
	private  StaffRepositery repository;
	
	
	@Override
	public CustomUserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {

	
	 StaffDetails user=this.repository.findByUserId(userId); 
	 
	 //StaffDetails user=user1.get();
		
		if(user==null) {
			throw  new UsernameNotFoundException("User Not Found.!");
		}
		else {
			return new CustomUserDetails(user);
		}
		
		//		
//	   if(userName.equals("pk")) {
//		   
//		 return new User("pk","pk12345", new ArrayList<>());
//		 
//		}else {
//			throw new UsernameNotFoundException("User Name Not found..?");
//		}
//		
//	}
	
	}
	

}
