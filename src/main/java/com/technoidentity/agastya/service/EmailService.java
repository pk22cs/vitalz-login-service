package com.technoidentity.agastya.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	public void sendEmail(String subject,String emailId,String message) throws MessagingException, IOException{

	       MimeMessage msg = javaMailSender.createMimeMessage();
	       MimeMessageHelper helper = new MimeMessageHelper(msg, true);
	       helper.setTo(emailId);
	       
           helper.setSubject(subject);
           helper.setText(message, true);

	      // helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

	       javaMailSender.send(msg);

	   }


}
