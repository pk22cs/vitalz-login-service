package com.technoidentity.agastya.repositery;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.technoidentity.agastya.model.Hospital;

public interface HospitalRepositery extends MongoRepository<Hospital,String>{

	
}
