package com.technoidentity.agastya.repositery;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.technoidentity.agastya.model.StaffDetails;

@Repository
public interface StaffRepositery extends MongoRepository<StaffDetails,String> {
 
	public StaffDetails findByUserId(String userId);

	public StaffDetails findByEmail(String email);
	
}
