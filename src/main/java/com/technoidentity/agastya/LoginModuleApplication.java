package com.technoidentity.agastya;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import com.technoidentity.agastya.model.Hospital;
import com.technoidentity.agastya.repositery.HospitalRepositery;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
@OpenAPIDefinition(info=@Info(title = "Login Module ",version = "1.0.0",
description = "This API for Login staff(ADMIN,DOCTOR,NURSE) and Patient_Caretatker | List Hospital & List of Patient Also there"))
@SpringBootApplication
public class LoginModuleApplication implements CommandLineRunner{
	
	  @Bean 
	  public WebClient.Builder getWebClintBuilder(){
	    WebClient.Builder builder=WebClient.builder();
		return builder;
	  }
	
	@Autowired
	private HospitalRepositery hospitalRepositery;
	
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(LoginModuleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	    
	/*	hospitalRepositery.insert(new Hospital("VIHP-111","Gandhi Hospital",new Address("Shiv Bagh Nager","Ameerper","Telangena", "500051")));
		hospitalRepositery.insert(new Hospital("VIHP-112","EYE Hospital",new Address("Shiv Bagh Nager","Ameerper","Telangena", "500053")));
		hospitalRepositery.insert(new Hospital("VIHP-113","Swati Hospital",new Address("Bulckum pet","Ameerper","Telangena", "500056")));
		hospitalRepositery.insert(new Hospital("VIHP-114"," Sankhya Hospitals ",new Address("Shiv Bagh Nager","Ameerper","Telangena", "500059")));
		hospitalRepositery.insert(new Hospital("VIHP-115","Srikara Hospitals ",new Address("Mythri Nagar","Ameerper","Telangena", "500052")));
	*/
		
	/*	  
	      repositery.insert(new PatientDetails("111","venketa","vk@gmail.com",Role.caretaker,"9967887639","+91","Gandhi Hospital","VIHP-111","9967887639","Saket Nager Bhopal",28,Gender.M));
	      repositery.insert(new PatientDetails("112","venketa","bk@gmail.com",Role.patient,"9494319679","+91","Sankhya Hospitals","VIHP-112","9494319679","Amerpet Hyderabad",29,Gender.M));
	      repositery.insert(new PatientDetails("113","Nikhil","nk@gmail.com",Role.caretaker,"7011187031","+91","Gandhi Hospital","VIHP-111","7011187031","Hanuman Nager Hyderabad",25,Gender.M));
	      repositery.insert(new PatientDetails("114","prince","sk@gmail.com",Role.caretaker,"7415520049","+91","Gandhi Hospital","VIHP-111","7415520049","Motihari Bihar",33,Gender.M));
			
	      repositery.insert(new PatientDetails("115","Akash","ak@gmail.com",Role.caretaker,"9967887631","+91","Gandhi Hospital","VIHP-111","9967887639","Saket Nager Bhopal",28,Gender.M));
	      repositery.insert(new PatientDetails("116","Deepak","bk@gmail.com",Role.caretaker,"9494319674","+91","Sankhya Hospitals","VIHP-112","9494319679","Amerpet Hyderabad",29,Gender.M));
	      repositery.insert(new PatientDetails("117","Ajay","nk@gmail.com",Role.caretaker,"7011187032","+91","Gandhi Hospital","VIHP-111","7011187031","Hanuman Nager Hyderabad",25,Gender.M));
	      repositery.insert(new PatientDetails("118","Nikhil","sk@gmail.com",Role.caretaker,"7415520044","+91","Gandhi Hospital","VIHP-111","7415520049","Motihari Bihar",33,Gender.M));
			
			
	      repositery.insert(new PatientDetails("119","Sonu","vk@gmail.com",Role.caretaker,"9967887632","+91","Gandhi Hospital","VIHP-111","9967887639","Saket Nager Bhopal",28,Gender.M));
	      repositery.insert(new PatientDetails("120","venketa","bk@gmail.com",Role.caretaker,"9494319673","+91","Sankhya Hospitals","VIHP-112","9494319679","Amerpet Hyderabad",29,Gender.M));
	      repositery.insert(new PatientDetails("121","Nikhil","nk@gmail.com",Role.caretaker,"7011187033","+91","Gandhi Hospital","VIHP-111","7011187031","Hanuman Nager Hyderabad",25,Gender.M));
	      repositery.insert(new PatientDetails("122","pk","sk@gmail.com",Role.caretaker,"7415520043","+91","Gandhi Hospital","VIHP-111","7415520049","Motihari Bihar",33,Gender.M));
			
			
	      repositery.insert(new PatientDetails("123","Ajit","vk@gmail.com",Role.caretaker,"9967887633","+91","Gandhi Hospital","VIHP-111","9967887639","Saket Nager Bhopal",28,Gender.M));
	      repositery.insert(new PatientDetails("124","Adarsh","bk@gmail.com",Role.caretaker,"9494319672","+91","Sankhya Hospitals","VIHP-112","9494319679","Amerpet Hyderabad",29,Gender.M));
	      repositery.insert(new PatientDetails("125","Susil","nk@gmail.com",Role.caretaker,"7011187034","+91","Gandhi Hospital","VIHP-111","7011187031","Hanuman Nager Hyderabad",25,Gender.M));
	      repositery.insert(new PatientDetails("126","prince","sk@gmail.com",Role.caretaker,"7415520042","+91","Gandhi Hospital","VIHP-111","7415520049","Motihari Bihar",33,Gender.M));
	*/
		
	   /*   repositery.insert(new PatientDetails("1","pappu","vk@gmail.com","caretaker","9967887634","+91","Gandhi Hospital","VIHP-111","9967887639","Saket Nager Bhopal",28,"M"));
	      repositery.insert(new PatientDetails("2","Akash","bk@gmail.com","caretaker","9494319671","+91","Sankhya Hospitals","VIHP-112","9494319679","Amerpet Hyderabad",29,"M"));
	      repositery.insert(new PatientDetails("3","Anil","nk@gmail.com","caretaker","7011187035","+91","Gandhi Hospital","VIHP-111","7011187031","Hanuman Nager Hyderabad",25,"M"));
	      repositery.insert(new PatientDetails("4","Sudhir","sk@gmail.com","caretaker","7415520041","+91","Gandhi Hospital","VIHP-111","7415520049","Motihari Bihar",33,"M"));
		*/	
			
		
		List<Hospital> hospitalList=hospitalRepositery.findAll();
		Iterator<Hospital> itr=hospitalList.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	   
	}

}
