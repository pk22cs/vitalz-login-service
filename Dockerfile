# Dockerfile for vitalz-graph-service  openjdk/openjdk-11-rhel7

FROM tiagastyainddevacr.azurecr.io/vitalzbaseimage:v1

#COPY . /vitalz-graph.jar

ARG JAR_FILE=build/libs/*.jar

COPY ${JAR_FILE} login.jar

#RUN bash -c 'touch target/app.jar'

EXPOSE 8023 80 443

CMD ["java", "-jar", "/login.jar"]